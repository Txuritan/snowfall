use {
    crate::{
        backend::{
            mysql::MysqlConnectionManager, postgres::PostgresConnectionManager,
            sqlite::SqliteConnectionManager,
        },
        config, CONFIG,
    },
    juniper::Context,
    r2d2::Pool,
};

#[derive(Clone)]
pub enum Database {
    MySQL {
        pool: Pool<MysqlConnectionManager>,
    },
    PostgreSQL {
        pool: Pool<PostgresConnectionManager>,
    },
    SQLite {
        pool: Pool<SqliteConnectionManager>,
    },
}

impl Database {
    pub fn new() -> Database {
        Default::default()
    }
}

impl Default for Database {
    fn default() -> Database {
        match &CONFIG.database {
            config::Database::MySQL { ref uri } => {
                let opts =
                    mysql::Opts::from_url(uri.as_str()).expect("Unable to connect to MySQL server");
                let opts_builder = mysql::OptsBuilder::from_opts(opts);
                let manager = MysqlConnectionManager::new(opts_builder);

                let pool = r2d2::Pool::new(manager).unwrap();

                Database::MySQL { pool }
            }
            config::Database::PostgreSQL { ref uri } => {
                let manager = PostgresConnectionManager::new(uri.as_str())
                    .expect("Unable to connect to PostgreSQL server");

                let pool = r2d2::Pool::new(manager).unwrap();

                Database::PostgreSQL { pool }
            }
            config::Database::SQLite { file } => {
                let manager = SqliteConnectionManager::file(file).with_init(|c| {
                    c.execute_batch(r#"PRAGMA encoding="UTF-8";PRAGMA foreign_keys=1;"#)
                });

                let pool = r2d2::Pool::new(manager).unwrap();

                {
                    let conn = pool.get().expect("Unable to get connection");

                    conn.execute_batch(
                        format!("BEGIN;\n{}\nCOMMIT;", include_str!("sql.sqlite")).as_str(),
                    )
                    .expect("Unable to setup database");
                }

                Database::SQLite { pool }
            }
        }
    }
}

impl Context for Database {}
