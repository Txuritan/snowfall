use postgres::{
    params::{ConnectParams, IntoConnectParams},
    Connection, Error, Result,
};

/// An `r2d2::ManageConnection` for `postgres::Connection`s.
///
/// ## Example
///
/// ```rust,no_run
/// extern crate r2d2;
/// extern crate r2d2_postgres;
///
/// use std::thread;
/// use r2d2_postgres::{TlsMode, PostgresConnectionManager};
///
/// fn main() {
///     let manager = PostgresConnectionManager::new("postgres://postgres@localhost",
///                                                  TlsMode::None).unwrap();
///     let pool = r2d2::Pool::new(manager).unwrap();
///
///     for i in 0..10i32 {
///         let pool = pool.clone();
///         thread::spawn(move || {
///             let conn = pool.get().unwrap();
///             conn.execute("INSERT INTO foo (bar) VALUES ($1)", &[&i]).unwrap();
///         });
///     }
/// }
/// ```
#[derive(Debug)]
pub struct PostgresConnectionManager {
    params: ConnectParams,
}

impl PostgresConnectionManager {
    /// Creates a new `PostgresConnectionManager`.
    ///
    /// See `postgres::Connection::connect` for a description of the parameter
    /// types.
    pub fn new<T>(params: T) -> Result<PostgresConnectionManager>
    where
        T: IntoConnectParams,
    {
        // FIXME we shouldn't be using this private constructor :(
        let params = params
            .into_connect_params()
            .map_err(postgres_shared::error::connect)?;

        Ok(PostgresConnectionManager { params })
    }
}

impl r2d2::ManageConnection for PostgresConnectionManager {
    type Connection = Connection;
    type Error = Error;

    fn connect(&self) -> Result<postgres::Connection> {
        postgres::Connection::connect(self.params.clone(), postgres::TlsMode::None)
    }

    fn is_valid(&self, conn: &mut Connection) -> Result<()> {
        conn.batch_execute("")
    }

    fn has_broken(&self, conn: &mut Connection) -> bool {
        conn.is_desynchronized()
    }
}
