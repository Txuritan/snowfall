mod backend;
mod config;
mod database;
mod schema;

use {
    crate::{
        config::Config,
        database::Database,
        schema::{create_schema, Schema},
    },
    actix_rt::System,
    actix_web::{middleware, web, App, Error, HttpResponse, HttpServer},
    futures::future::Future,
    juniper::http::GraphQLRequest,
    std::{io, sync::Arc},
};

lazy_static::lazy_static! {
    pub static ref CONFIG: Config = Config::load();
}

#[derive(Clone)]
struct SnowfallState {
    database: Database,
    graphql: Arc<Schema>,
}

fn main() -> io::Result<()> {
    ::std::env::set_var("RUST_LOG", "info");
    env_logger::init();

    let sys = System::new("snowfall");

    let data = SnowfallState {
        database: Database::new(),
        graphql: Arc::new(create_schema()),
    };

    let addr = format!("{}:{}", CONFIG.server.host, CONFIG.server.port);

    HttpServer::new(move || {
        App::new()
            .data(data.clone())
            .wrap(middleware::Logger::default())
            .service(web::resource("/graphql").route(web::post().to_async(graphql)))
    })
    .bind(addr)?
    .start();

    sys.run()
}

fn graphql(
    (state, data): (web::Data<SnowfallState>, web::Json<GraphQLRequest>),
) -> impl Future<Item = HttpResponse, Error = Error> {
    web::block(move || {
        let res = data.execute(&state.graphql, &state.database);
        Ok::<_, serde_json::error::Error>(serde_json::to_string(&res)?)
    })
    .map_err(Error::from)
    .and_then(|res| {
        Ok(HttpResponse::Ok()
            .content_type("application/json")
            .body(res))
    })
}
