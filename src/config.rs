use {
    ron::{
        de::from_reader,
        ser::{to_string_pretty, PrettyConfig},
    },
    serde::{Deserialize, Serialize},
    std::{
        fs::File,
        io::{prelude::*, BufReader},
        path::Path,
    },
};

#[derive(Clone, Deserialize, Serialize)]
pub struct Config {
    pub jwt: String,
    pub salt: String,
    #[serde(default = "Default::default")]
    pub database: Database,
    #[serde(default = "Default::default")]
    pub server: Server,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            jwt: "<CHANGE ME>".into(),
            salt: "<CHANGE ME>".into(),
            database: Database::default(),
            server: Server::default(),
        }
    }
}

#[derive(Clone, Deserialize, Serialize)]
pub enum Database {
    MySQL { uri: String },
    PostgreSQL { uri: String },
    SQLite { file: String },
}

impl Default for Database {
    fn default() -> Self {
        Database::SQLite {
            file: "snowfall.db".into(),
        }
    }
}

#[derive(Clone, Deserialize, Serialize)]
pub struct Server {
    pub host: String,
    pub port: String,
}

impl Default for Server {
    fn default() -> Self {
        Self {
            host: "127.0.0.1".into(),
            port: "50411".into(),
        }
    }
}

impl Config {
    pub fn load() -> Config {
        let config = Path::new("config.ron");

        if config.exists() {
            let config = from_reader::<_, Config>(BufReader::new(
                File::open(config).expect("unable to open config file"),
            ))
            .expect("failed to load config");

            if config.jwt == "<CHANGE ME>"
                || config.salt == "<CHANGE ME>"
                || config.jwt.is_empty()
                || config.salt.is_empty()
            {
                panic!("Please change the jwt and salt fields in the config before continuing");
            }

            config
        } else {
            let mut file = File::create(config).expect("Unable to create config file");

            file.write_all(
                to_string_pretty(&Config::default(), PrettyConfig::default())
                    .expect("Unable to write config to string")
                    .as_bytes(),
            )
            .expect("Unable to write to config file");

            panic!("Please change the jwt and salt fields in the config before continuing");
        }
    }
}
