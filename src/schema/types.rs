use {
    juniper::GraphQLObject,
    serde::{Deserialize, Serialize},
};

#[derive(GraphQLObject, Serialize, Deserialize)]
pub struct PageInfo {
    #[serde(rename = "hasNextPage")]
    pub next_page: bool,
    #[serde(rename = "hasPreviousPage")]
    pub previous_page: bool,
    #[serde(rename = "startCursor")]
    pub start_cursor: Option<String>,
    #[serde(rename = "endCursor")]
    pub end_cursor: Option<String>,
}

#[derive(GraphQLObject, Serialize, Deserialize)]
/// A user's bookmark
pub struct Bookmark {
    pub id: i32,
    pub title: String,
    pub domain: String,
    pub url: String,

    pub tags: Vec<Tag>,
}

#[derive(GraphQLObject, Serialize, Deserialize)]
pub struct BookmarkConnection {
    #[serde(rename = "pageInfo")]
    page_info: PageInfo,
    edges: Option<Vec<BookmarkEdge>>,
    #[serde(rename = "totalCount")]
    total_count: Option<i32>,
    bookmarks: Option<Vec<Bookmark>>,
}

#[derive(GraphQLObject, Serialize, Deserialize)]
pub struct BookmarkEdge {
    cursor: String,
    node: Option<Bookmark>,
}

#[derive(GraphQLObject, Serialize, Deserialize)]
/// A user's collection of bookmark
pub struct Collection {
    pub id: i32,
    pub title: String,
}

#[derive(GraphQLObject, Serialize, Deserialize)]
/// A bookmark's tag
pub struct Tag {
    pub id: i32,
    pub name: String,
}

#[derive(GraphQLObject, Serialize, Deserialize)]
pub struct User {
    pub id: String,
    pub username: String,
    pub email: String,
}

#[derive(GraphQLObject, Serialize, Deserialize)]
pub struct Login {
    pub token: String,
}

#[derive(GraphQLObject, Serialize, Deserialize)]
pub struct Logout {}

#[derive(GraphQLObject, Serialize, Deserialize)]
pub struct Register {
    pub token: String,
}
