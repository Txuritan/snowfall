use {
    crate::{
        database::Database,
        schema::{types::*, MutationRoot},
        CONFIG,
    },
    chrono::{DateTime, Utc},
    juniper::{FieldError, FieldResult, Value},
    rusqlite::{types::ToSql, OptionalExtension},
    url::Url,
};

juniper::graphql_object!(MutationRoot: Database |&self| {
    field createBookmark(
        &executor,
        token: String,
        title: String,
        url: String,
        tags: Option<Vec<String>>,
    ) -> FieldResult<Bookmark> {
        executor.context().create_bookmark(token, title, url, tags)
    }

    field register(
        &executor,
        username: String,
        email: String,
        password: String,
    ) -> FieldResult<Register> {
        executor.context().register(username, email, password)
    }

    field login(
        &executor,
        email: String,
        password: String,
    ) -> FieldResult<Login> {
        executor.context().login(email, password)
    }
});

impl Database {
    fn create_bookmark(
        &self,
        token: String,
        title: String,
        url: String,
        tags: Option<Vec<String>>,
    ) -> FieldResult<Bookmark> {
        match self {
            //#region[rgba(241,153,31,0.1)] MySQL
            Database::MySQL { .. } => Ok(Bookmark {
                id: 0,
                title: "".to_string(),
                domain: "".to_string(),
                url: "".to_string(),
                tags: Vec::new(),
            }),
            //#endregion

            //#region[rgba(51,103,145,0.1)] PostgreSQL
            Database::PostgreSQL { .. } => Ok(Bookmark {
                id: 0,
                title: "".to_string(),
                domain: "".to_string(),
                url: "".to_string(),
                tags: Vec::new(),
            }),
            //#endregion

            //#region[rgba(1,52,76,0.3)] SQLite
            Database::SQLite { pool } => {
                let mut conn = pool.get()?;

                let tx = conn.transaction()?;

                let parsed = Url::parse(&url)?;
                let host = parsed.host();

                let date_time: DateTime<Utc> = Utc::now();
                let naive = date_time.naive_utc();

                let bookmark_id: i32;

                {
                    tx.execute(
                        "INSERT INTO bookmarks(title, domain, url, created, last_updated) VALUES (?1, ?2, ?3, ?4, ?5);",
                        &[
                            &title as &ToSql,
                            &if let Some(host) = host {
                                host.to_string()
                            } else {
                                "UNKNOWN".to_string()
                            } as &ToSql,
                            &url as &ToSql,
                            &naive as &ToSql,
                            &naive as &ToSql,
                        ],
                    )?;

                    bookmark_id = tx.last_insert_rowid() as i32;

                    if let Some(tags) = tags {
                        let mut check_stmt = tx.prepare("SELECT id FROM tags WHERE name = ?1;")?;

                        let mut inst_tag_stmt = tx.prepare(
                            "INSERT INTO tags (name, created, last_updated) VALUES (?1, ?2, ?3);",
                        )?;

                        let mut inst_bk_tag_stmt = tx.prepare(
                            "INSERT OR IGNORE INTO bookmark_tags (bookmark_id, tag_id, created, last_updated) VALUES (?1, ?2, ?3, ?4);"
                        )?;

                        for tag in tags {
                            let tag = tag.to_lowercase();
                            let tag = tag.trim();

                            let id = if let Some(id) = check_stmt
                                .query_row(&[&tag as &ToSql], |row| Ok(row.get::<&str, i32>("id")))
                                .optional()?
                            {
                                id?
                            } else {
                                inst_tag_stmt.execute(&[
                                    &tag as &ToSql,
                                    &naive as &ToSql,
                                    &naive as &ToSql,
                                ])?;

                                tx.last_insert_rowid() as i32
                            };

                            inst_bk_tag_stmt.execute(&[
                                &bookmark_id as &ToSql,
                                &id as &ToSql,
                                &naive as &ToSql,
                                &naive as &ToSql,
                            ])?;
                        }
                    }
                }

                tx.commit()?;

                self.get_bookmark_by_id(Some(token), bookmark_id)
            } //#endregion
        }
    }

    fn register(&self, username: String, email: String, password: String) -> FieldResult<Register> {
        fast_chemail::parse_email(&email)?;

        match self {
            //#region[rgba(241,153,31,0.1)] MySQL
            Database::MySQL { .. } => Ok(Register { token: "".into() }),
            //#endregion

            //#region[rgba(51,103,145,0.1)] PostgreSQL
            Database::PostgreSQL { .. } => Ok(Register { token: "".into() }),
            //#endregion

            //#region[rgba(1,52,76,0.3)] SQLite
            Database::SQLite { pool } => {
                let mut conn = pool.get()?;

                if conn
                    .query_row(
                        "SELECT email FROM users WHERE email = ?;",
                        &[&email as &ToSql],
                        |_| Ok(true),
                    )
                    .optional()?
                    .is_some()
                {
                    Err(FieldError::new("Email is already in use", Value::null()))
                } else {
                    let salt = nanoid::generate(64);
                    let hash = bcrypt::hash(
                        format!("{}-{}-{}", CONFIG.salt, password, salt),
                        bcrypt::DEFAULT_COST,
                    )?;

                    let date_time: DateTime<Utc> = Utc::now();
                    let naive = date_time.naive_utc();

                    let tx = conn.transaction()?;

                    {
                        tx.execute(
                            "INSERT INTO users (id, name, email, salt, hash, created, last_updated) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7)",
                            &[
                                &uuid::Uuid::new_v4().to_string() as &ToSql,
                                &username as &ToSql,
                                &email as &ToSql,
                                &salt as &ToSql,
                                &hash as &ToSql,
                                &naive as &ToSql,
                                &naive as &ToSql,
                            ]
                        )?;
                    }

                    tx.commit()?;

                    Ok(Register {
                        token: self.login(email, password)?.token,
                    })
                }
            } //#endregion
        }
    }

    fn login(&self, email: String, password: String) -> FieldResult<Login> {
        fast_chemail::parse_email(&email)?;

        match self {
            //#region[rgba(241,153,31,0.1)] MySQL
            Database::MySQL { .. } => Ok(Login { token: "".into() }),
            //#endregion

            //#region[rgba(51,103,145,0.1)] PostgreSQL
            Database::PostgreSQL { .. } => Ok(Login { token: "".into() }),
            //#endregion

            //#region[rgba(1,52,76,0.3)] SQLite
            Database::SQLite { pool } => {
                let conn = pool.get()?;

                if let Some((id, name, salt, hash)) = conn
                    .query_row(
                        "SELECT id, name, salt, hash FROM users WHERE email = ?1;",
                        &[&email as &ToSql],
                        |row| {
                            Ok((
                                row.get::<_, String>("id")?,
                                row.get::<_, String>("name")?,
                                row.get::<_, String>("salt")?,
                                row.get::<_, String>("hash")?,
                            ))
                        },
                    )
                    .optional()?
                {
                    if bcrypt::verify(format!("{}-{}-{}", CONFIG.salt, password, salt), &hash)? {
                        Ok(Login {
                            token: frank_jwt::encode(
                                serde_json::json!({}),
                                &CONFIG.jwt,
                                &serde_json::json!({
                                    "id": id,
                                    "name": name,
                                    "email": email,
                                }),
                                frank_jwt::Algorithm::HS256,
                            )?,
                        })
                    } else {
                        Err(FieldError::new(
                            "Email or password is incorrect",
                            Value::null(),
                        ))
                    }
                } else {
                    Err(FieldError::new(
                        "Email or password is incorrect",
                        Value::null(),
                    ))
                }
            } //#endregion
        }
    }
}
