use {
    crate::{
        database::Database,
        schema::{types::*, QueryRoot},
    },
    juniper::FieldResult,
    rusqlite::types::ToSql,
};

juniper::graphql_object!(QueryRoot: Database |&self| {
    field bookmark(&executor, token: Option<String>, id: i32) -> FieldResult<Bookmark> {
        executor.context().get_bookmark_by_id(token, id)
    }

    field collection(&executor, token: Option<String>, id: i32) -> FieldResult<Collection> {
        match executor.context() {
            //#region[rgba(241,153,31,0.1)] MySQL
            Database::MySQL { pool } => {
                Ok(Collection {
                    id,
                    title: "4".to_string(),
                })
            },
            //#endregion

            //#region[rgba(51,103,145,0.1)] PostgreSQL
            Database::PostgreSQL { pool } => {
                Ok(Collection {
                    id,
                    title: "4".to_string(),
                })
            },
            //#endregion

            //#region[rgba(1,52,76,0.3)] SQLite
            Database::SQLite { pool } => {
                Ok(Collection {
                    id,
                    title: "4".to_string(),
                })
            },
            //#endregion
        }
    }

    field tag(&executor, token: Option<String>, id: i32) -> FieldResult<Tag> {
        match executor.context() {
            //#region[rgba(241,153,31,0.1)] MySQL
            Database::MySQL { pool } => {
                Ok(Tag {
                    id,
                    name: "4".to_string(),
                })
            },
            //#endregion

            //#region[rgba(51,103,145,0.1)] PostgreSQL
            Database::PostgreSQL { pool } => {
                Ok(Tag {
                    id,
                    name: "4".to_string(),
                })
            },
            //#endregion

            //#region[rgba(1,52,76,0.3)] SQLite
            Database::SQLite { pool } => {
                Ok(Tag {
                    id,
                    name: "4".to_string(),
                })
            },
            //#endregion
        }
    }
});

impl Database {
    pub fn get_bookmark_by_id(&self, _token: Option<String>, id: i32) -> FieldResult<Bookmark> {
        match self {
            //#region[rgba(241,153,31,0.1)] MySQL
            Database::MySQL { .. } => Ok(Bookmark {
                id,
                title: "".to_string(),
                domain: "".to_string(),
                url: "".to_string(),
                tags: Vec::new(),
            }),
            //#endregion

            //#region[rgba(51,103,145,0.1)] PostgreSQL
            Database::PostgreSQL { .. } => Ok(Bookmark {
                id,
                title: "".to_string(),
                domain: "".to_string(),
                url: "".to_string(),
                tags: Vec::new(),
            }),
            //#endregion

            //#region[rgba(1,52,76,0.3)] SQLite
            Database::SQLite { pool } => {
                let conn = pool.get()?;

                let mut bookmark = conn.query_row(
                    "SELECT title, domain, url FROM bookmarks WHERE id = ?;",
                    &[&id as &ToSql],
                    |row| {
                        Ok(Bookmark {
                            id,
                            title: row.get("title")?,
                            domain: row.get("domain")?,
                            url: row.get("url")?,
                            tags: Vec::new(),
                        })
                    },
                )?;

                let mut stmt = conn.prepare(
                    "SELECT tag.id, tag.name FROM bookmark_tags bookmark_tag LEFT JOIN tags tag ON bookmark_tag.tag_id = tag.id WHERE bookmark_tag.bookmark_id = ?1 ORDER BY tag.name;"
                )?;

                for tag in stmt.query_map(&[&bookmark.id as &ToSql], |row| {
                    Ok(Tag {
                        id: row.get(0)?,
                        name: row.get(1)?,
                    })
                })? {
                    bookmark.tags.push(tag?);
                }

                Ok(bookmark)
            } //#endregion
        }
    }
}
